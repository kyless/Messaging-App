import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Platform,
  Text,
} from "react-native";
import { Icon } from "expo";

export default class InfoScreenVerseContainer extends Component {
  render() {
    const {
      backgroundColor,
      verse,
      verseFontSize,
      verseReference,
    } = this.props;

    return(
      <View style={[styles.verseContainer, {backgroundColor: backgroundColor}]}>
        <View style={styles.textContainer}>
          <Text style={ {fontSize: parseInt(verseFontSize)}}>
            {verse}
          </Text>
          <View style={styles.referenceContainer}>
            <Icon.Ionicons
              name={ Platform.OS === "ios" ? "ios-open-outline" : "md-open" }
              size={20}/>
            <Text style={styles.verseReference}>{verseReference}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  referenceContainer: {
    alignItems: "center",
    flexDirection: "row",
  },
  textContainer: {
    margin: 15,
  },
  verseContainer: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    margin: 15,
  },
  verseReference: {
    fontWeight: "bold",
    marginLeft: 10,
  },
});
