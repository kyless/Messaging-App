import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Platform,
} from 'react-native';
import { Icon } from 'expo';

export default class InfoScreenHeader extends Component {
  render() {
    const {
      headerTitle,
      navigation,
    } = this.props;

    return(
      <View style={styles.header}>
        <Text style={styles.headerTitle}>{headerTitle}</Text>
        <Icon.Ionicons style={styles.closeIcon}
          name={ Platform.OS === 'ios' ? 'ios-close' : 'md-close' }
          size={30}
          onPress={() => navigation.goBack()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  headerTitle: {
    fontSize: 22,
    fontWeight: 'bold',
    marginTop: 40,
    marginLeft: 20,
  },
  closeIcon: {
    marginTop: 40,
    marginRight: 20,
  },
});
