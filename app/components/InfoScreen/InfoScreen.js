import React, {Component} from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
} from "react-native";

import InfoScreenHeader from "./InfoScreenHeader";
import InfoScreenVerseContainer from "./InfoScreenVerseContainer";

export default class InfoScreen extends Component {
  render() {
    const {
      backgroundColor,
      explanationText,
      explanationFontSize,
      headerTitle,
      imagePath,
      navigation,
      verse,
      verseContainerColor,
      verseFontSize,
      verseReference,
    } = this.props;

    return(
      <View style={[styles.screen, {backgroundColor: backgroundColor}]}>
        <InfoScreenHeader headerTitle={headerTitle} navigation={navigation}/>
        <View style={styles.container}>
          <Image style={styles.image} source={imagePath}/>
          <View style={styles.explanationContainer}>
            <Text style={[styles.explanationText, {fontSize: parseInt(explanationFontSize)}]}>
              {explanationText}
            </Text>
          </View>
          <InfoScreenVerseContainer 
            backgroundColor={verseContainerColor}
            verse={verse} 
            verseFontSize={verseFontSize}
            verseReference={verseReference}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    alignItems: "center",
    flex: 1,
  },
  explanationContainer: {
    flex: 1,
    margin: 20,
  },
  explanationText: {
    textAlign: "center",
  },
  image: {
    flex: 1,
    marginTop: 30,
    marginBottom: 20,
  },
  screen: {
    flex: 1,
    flexDirection: "column",
  },
 
});
