import React, { Component } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';

import Colors from '@root/constants/Colors';

export default class TabBarIcon extends Component {
  render() {
    return (
      <View style={styles.style}
        backgroundColor={this.props.color}
      />
    );
  }
}

const styles = StyleSheet.create({
  style: {
    height: 6,
    width: 6,
    borderRadius: 3,
  }
});
