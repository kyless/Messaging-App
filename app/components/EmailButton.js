import React, { Component } from 'react';
import {
  TouchableOpacity,
  View,
  Icon,
  Text
} from 'react-native';

export default class EmailButton extends Component {
  render() {
    return(
      <TouchableOpacity onPress={() => {this.props.onPress}}>
        <View>
          <Icon/>
          <Text/>
        </View>
      </TouchableOpacity>
    );
  };
}
