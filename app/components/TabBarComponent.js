import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';

import TabBarContainer from './TabBarContainer';

export default class TabBarComponent extends Component {
  render() {
    return(
      <View style={styles.tabBarContainer}>
        <TabBarContainer
          text="Pray"
          color="#fe6067"
        />
        <TabBarContainer
          text="Share"
          color="#feda26"
        />
        <TabBarContainer
          text="Fellowship"
          color="#e2d0b9"
        />
        <TabBarContainer
          text="Grow"
          color="#0fcdaf"
        />
        <TabBarContainer
          text="Go"
          color="#6799ff"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tabBarContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: 70,
    backgroundColor: '#e8e8e8',
  },
});
