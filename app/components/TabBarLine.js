import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

export default class TabBarLine extends Component {
  render() {
    const {
      color,
    } = this.props;

    return(
      <View style={styles.style}
        backgroundColor={color}
      />
    );
  }
}

const styles = StyleSheet.create({
  style: {
    height: 3,
  },
});
