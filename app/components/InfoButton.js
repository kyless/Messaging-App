import React, { Component } from 'react';
import {
  Platform,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { Icon } from 'expo';

export default class InfoButton extends Component {
    render() {
      const {
        onPress
      } = this.props;
      return (
        <TouchableOpacity style={styles.button}
        onPress={onPress}>
          <Icon.Ionicons
            name= {Platform.OS === 'ios' ? 'ios-information-circle-outline' : 'md-information-circle'}
            size= {26}
          />
        </TouchableOpacity>
      );
    }
}

const styles = StyleSheet.create({
  button: {
    padding: 10,
  }
});
