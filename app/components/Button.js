import React, { Component } from "react";
import {
  StyleSheet,
  Platform,
  TouchableHighlight,
  TouchableNativeFeedback,
  View,
  Text
} from "react-native";
import { Icon } from "expo";

export default class Button extends Component {
  render() {
    const {
      accessibilityLabel,
      buttonStyle,
      disabled,
      imageColor,
      imageName,
      imageSize,
      imageStyle,
      onPress,
      testID,
      textStyle,
      title,      
    } = this.props;

    const Touchable =
      Platform.OS === "android" ? TouchableNativeFeedback : TouchableHighlight;
    return (
      <Touchable
        accessibilityLabel={accessibilityLabel}
        accessibilityRole="button"
        testID={testID}
        disabled={disabled}
        onPress={onPress}>
        <View style={buttonStyle}>
          <Text style={textStyle} disabled={disabled}>
            {title}
          </Text>
          <Icon.Ionicons style={imageStyle}
            name={imageName}
            size={parseInt(imageSize)}
            color={imageColor}
          />
        </View>
      </Touchable>
    );
  }
}