import React, { Component } from "react";
import {
  Platform,
  TouchableOpacity,
  StyleSheet,
} from "react-native";
import { Icon } from "expo";

export default class MenuButton extends Component {
    render() {
      const {
        buttonStyle,
        onPress,
      } = this.props;
      if (Platform.OS == "ios") {
        return (
          <TouchableOpacity style={buttonStyle}
            onPress={onPress}>
            <Icon.Ionicons style={styles.iosIcon}
              name= {Platform.OS === "ios" ? "ios-more" : "md-more"}
              size= {26}
            />
          </TouchableOpacity>
        );
      } 
      else {
        return(
          <TouchableOpacity style={buttonStyle}
            onPress={onPress}>
            <Icon.Ionicons
              name= {Platform.OS === "ios" ? "ios-more" : "md-more"}
              size= {26}
            />
          </TouchableOpacity>
        );
      }
    }
}

const styles = StyleSheet.create({
  iosIcon: {
    transform: [{ rotate: "90deg"}]
  },
});
