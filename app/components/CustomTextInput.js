import React, { Component } from 'react';
import {
  TextInput,
  StyleSheet,
} from 'react-native';

export default class CustomTextInput extends React.Component {
  render() {
    return(
      <TextInput
        style={styles.style}
        placeholder={this.props.placeholder}
        height={this.props.height}
        width={this.props.width}
      />
    );
  }
}

const styles = StyleSheet.create({
  style: {

  }
})
