import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableNativeFeedback,
  TouchableHighlight,
  Platform,
} from 'react-native';

import TabBarLine from './TabBarLine';

export default class TabBarContainer extends Component {
  render() {
    const Touchable =
      Platform.OS === 'android' ? TouchableNativeFeedback : TouchableHighlight;
    const {
      text,
      color,
      onClick,
    } = this.props;
    return(
      <Touchable style={styles.container}
        onClick={onClick}>
        <Text>{text}</Text>
      </Touchable>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'flex-end',
    alignSelf: 'stretch',
    borderBottomColor: "#000000",
    borderBottomWidth: 3,
  },
});
