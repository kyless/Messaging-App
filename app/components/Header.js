import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import InfoButton from './InfoButton';

export default class Header extends Component {
  render() {
    const {
      title,
      onPress
    } = this.props;
    return(
      <View style={styles.header}>
        <Text style={styles.title}>{title}</Text>
        <InfoButton style={styles.infoButton}
          onPress={onPress}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    marginLeft: 20,
    fontSize: 22,
    fontWeight: 'bold',
  },
});
