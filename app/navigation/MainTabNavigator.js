import React from 'react';
import { Platform , StyleSheet, View} from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import TabBarComponent from '../components/TabBarComponent';
import TabBarContainer from '../components/TabBarContainer';

import PrayScreen from '@root/views/PrayScreen/PrayScreen';
import ShareScreen from '@root/views/ShareScreen/ShareScreen';
import FellowshipScreen from '@root/views/FellowshipScreen/FellowshipScreen';
import GrowScreen from '@root/views/GrowScreen/GrowScreen';
import GoScreen from '@root/views/GoScreen/GoScreen';

const PrayerStack = createStackNavigator({
  Pray: PrayScreen,
});

PrayerStack.navigationOptions = {
  tabBarLabel: 'Pray',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      color="#fe6067"
    />
  ),
};

const ShareStack = createStackNavigator({
  Share: ShareScreen,
});

ShareStack.navigationOptions = {
  tabBarLabel: 'Share',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      color="#feda26"
    />
  ),
};

const FellowshipStack = createStackNavigator({
  Fellowship: FellowshipScreen,
});

FellowshipStack.navigationOptions = {
  tabBarLabel: 'Fellowship',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      color="#e2d0b9"
    />
  ),
};

const GrowStack = createStackNavigator({
  Grow: GrowScreen,
});

GrowStack.navigationOptions = {
  tabBarLabel: 'Grow',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      color="#0fcdaf"
    />
  ),
}

const GoStack = createStackNavigator({
  Go: GoScreen,
});

GoStack.navigationOptions = {
  tabBarLabel: 'Go',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      color="#6799ff"
    />
  ),
}

const styles = StyleSheet.create({
  prayTab: {
    borderBottomWidth: 3,
    borderBottomColor: "#FFFFFF",
  }
});

export default createBottomTabNavigator(
  {
    PrayerStack,
    ShareStack,
    FellowshipStack,
    GrowStack,
    GoStack,
  },
  {
    tabBarOptions: {
      activeTintColor: "#000000",
      inactiveTintColor: "#989898",
      showIcon: false,
      labelStyle: {
        fontSize: 15,
      },
      style: {
        backgroundColor: "#e8e8e8",
      },
    },
  },
);
