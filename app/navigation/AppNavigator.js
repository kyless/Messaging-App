import { createSwitchNavigator, createStackNavigator, createAppContainer } from "react-navigation";

import MainTabNavigator from "./MainTabNavigator";

import LoginScreen from "@root/views/LoginScreen/LoginScreen";
import EmailLoginScreen from "@root/views/LoginScreen/EmailLoginScreen";
import AuthLoadingScreen from "@root/views/LoginScreen/AuthLoadingScreen";
import PrayInfoScreen from "@root/views/PrayScreen/PrayInfoScreen";
import ShareInfoScreen from "@root/views/ShareScreen/ShareInfoScreen";
import FellowshipInfoScreen from "@root/views/FellowshipScreen/FellowshipInfoScreen";
import GrowInfoScreen from "@root/views/GrowScreen/GrowInfoScreen";
import GoInfoScreen from "@root/views/GoScreen/GoInfoScreen";
import MainMenuScreen from "@root/views/MainMenu/MainMenuScreen";
import GroupScreen from "@root/views/MainMenu/GroupScreen";
import ProfileScreen from "@root/views/MainMenu/ProfileScreen";
import InviteScreen from "@root/views/MainMenu/InviteScreen";
import AboutScreen from "@root/views/MainMenu/AboutScreen";

const AuthStack = createStackNavigator({
  Login: LoginScreen,
  EmailLogin: EmailLoginScreen,
});

const AppStack = createStackNavigator({
  App: MainTabNavigator,
  PrayInfo: PrayInfoScreen,
  ShareInfo: ShareInfoScreen,
  FellowshipInfo: FellowshipInfoScreen,
  GrowInfo: GrowInfoScreen,
  GoInfo: GoInfoScreen,
  MainMenu: MainMenuScreen,
  Group: GroupScreen,
  Profile: ProfileScreen,
  Invite: InviteScreen,
  About: AboutScreen,
},
{
  headerMode: "none",
  initialRouteName: "About",
}
);

export default createAppContainer(createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  App: AppStack,
  Auth: AuthStack,
},
{
  initialRouteName: "App",
},
));
