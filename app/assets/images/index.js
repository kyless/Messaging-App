const images = {
    prayer: require('./prayer/prayer.png'),
    share: require('./share/share.png'),
    fellowship: require('./fellowship/fellowship.png'),
    grow: require('./grow/grow.png'),
    go: require('./go/go.png'),
};

export default images;
