import React, { Component } from "React";
import { 
  View, 
  StyleSheet, 
  Platform,
} from "react-native";

import LogoIcon from "@root/components/LogoIcon";
import Button from "@root/components/Button";
import Colors from "@root/constants/Colors";

export default class LoginScreen extends Component {
  static navigationOptions = {
    header: null
  }

  render() {
    return(
      <View style={styles.container}>
        <LogoIcon style={styles.logoIcon}/>
        <View style={styles.buttonContainer}>
          <Button 
            onPress={() => {this.props.navigation.navigate("EmailLogin")}}
            accessibilityLabel="Login with Email"
            buttonStyle={styles.button}
            imageColor="#FFFFFF"
            imageName={
              Platform.OS === "ios"
                ? "ios-mail"
                : "md-mail"
            }
            imageSize="40"
            imageStyle={styles.image}
            textStyle={styles.text}
            title="Login with Email"
          />
          <Button
            onPress={() => {this.onPressFacebookLogin()}}
            accessibilityLabel="Login with Facebook"
            buttonStyle={styles.button}
            imageColor={Colors.facebookButton}
            imageName="logo-facebook"
            imageSize="40"
            imageStyle={styles.image}
            textStyle={styles.text}
            title="Login with Facebook"
          />
          <Button
            onPress={() => {this.onPressTwitterLogin()}}
            accessibilityLabel="Login with Twitter"
            buttonStyle={styles.button}
            imageColor={Colors.twitterButton}
            imageName="logo-twitter"
            imageSize="40"
            imageStyle={styles.image}
            textStyle={styles.text}
            title="Login with Twitter"
          />
          <Button
            onPress={() => {this.onPressGoogleLogin()}}
            accessibilityLabel="Login with Google"
            buttonStyle={styles.button}
            imageColor={Colors.googleButton}
            imageName="logo-google"
            imageSize="40"
            imageStyle={styles.image}
            textStyle={styles.text}
            title="Login with Google"
          />
        </View>
      </View>
    );
  }

  onPressEmailLogin = () => {
    this.props.navigation.navigate("EmailLogin");
  }

  onPressFacebookLogin = () => {
    console.log("Facebook not Implemented");
  }

  onPressTwitterLogin = () => {
    console.log("Twitter not Implemented");
  }

  onPressGoogleLogin = () => {
    console.log("Google not Implemented");
  }
}

const styles = StyleSheet.create({
  button: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    elevation: 4,
    backgroundColor: "#000000",
    borderRadius: 10,
    height: 50,
  },
  buttonContainer: {
    flexDirection: "column",
    flex: 1,
    justifyContent: "space-evenly",
    alignSelf: "stretch",
    padding: 20,
  },
  container: {
    backgroundColor: "#F4F4F4",
    flexDirection: "column",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 50,
  },
  image: {
    marginEnd: 20,
  },
  text: {
    fontWeight: "bold",
    textAlign: "left",
    marginStart: 20,
    color: "#ffffff"
  },
});