import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  KeyboardAvoidingView,
  AsyncStorage
 } from 'react-native';

import Button from '@root/components/Button';

export default class EmailLoginScreen extends Component {

  static navigationOptions = {
      title: 'Sign In',
  };

  render() {

    return(
      <KeyboardAvoidingView style = {styles.textInputContainer}
        behavior = 'padding'
      >
            <TextInput style={styles.textInput}
              placeholder='Name'
              placeholderTextColor="#000000"
              returnKeyType='next'
              onSubmitEditing={() => this.refs.textInputPhoneNumber.focus()}
              underlineColorAndroid='rgba(0,0,0,0)'
            />
            <TextInput style={styles.textInput}
              placeholder='Phone Number'
              placeholderTextColor="#000000"
              keyboardType='phone-pad'
              returnKeyType='next'
              ref={"textInputPhoneNumber"}
              onSubmitEditing={() => this.refs.textInputEmail.focus()}
              underlineColorAndroid='rgba(0,0,0,0)'
            />
            <TextInput style={styles.textInput}
              placeholder='Email'
              placeholderTextColor="#000000"
              keyboardType='email-address'
              returnKeyType='next'
              autoCorrect={false}
              autoCapitalize='none'
              ref={"textInputEmail"}
              onSubmitEditing={() => this.refs.textInputPassword.focus()}
              underlineColorAndroid='rgba(0,0,0,0)'
            />
            <TextInput style={styles.textInput}
              placeholder='Password'
              placeholderTextColor="#000000"
              returnKeyType='go'
              secureTextEntry={true}
              ref={"textInputPassword"}
              onSubmitEditing={() => this._signInAsync()}
              underlineColorAndroid='rgba(0,0,0,0)'
            />
            <Button
              title="Login"
              onPress={() => {this.props.navigation.navigate('App')}}
            />
        </KeyboardAvoidingView>
    );
  }

  _signInAsync = async () => {
    await AsyncStorage.setItem('userToken', 'abc');
    this.props.navigation.navigate('App');
  };
}

const styles = StyleSheet.create({
  textInputContainer: {
    backgroundColor: "#F4F4F4",
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 20,
  },
  textInput: {
    height: 40,
    backgroundColor: '#ffffff',
    paddingHorizontal: 10,
    marginBottom: 10,
  },
})
