import React, { Component } from "react";

import InfoScreen from "@root/components/InfoScreen/InfoScreen";
import Images from "@root/assets/images/index";
import Colors from "@root/constants/Colors";

export default class GoInfoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      explanation: "The purpose of the this chanel is to share opportunities " +
      "to live out the great commision.  Schedule information about short term " +
      "mission trips, or start your own local outreach opportunity. It could " +
      "be as easy as going to the park!",
      verse: "How then will they call on him in whom they have not believed? " +
      "And how are they to believe in him of whom they have never heard? And " +
      "how are they to hear without someone preaching? And how are they to " +
      "preach unless they are sent?",
      verseReference: "Acts 2:42",
    };
  }

  render() {
    return(
      <InfoScreen
        backgroundColor={Colors.go}
        explanationFontSize="20"
        explanationText={this.state.explanation}
        headerTitle="Go"
        imagePath={Images.go}
        navigation={this.props.navigation}
        verse={this.state.verse}
        verseContainerColor={Colors.goTint}
        verseFontSize="20"
        verseReference={this.state.verseReference}
      />
    );
  }
}
