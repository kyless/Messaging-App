import React, { Component } from "react";

import Header from "@root/components/Header";
import MenuButton from "@root/components/MenuButton";
import MessagingScreen from "@root/components/MessagingScreen";
import Colors from "@root/constants/Colors";

export default class GoScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: {
          backgroundColor: Colors.go,
      },
      headerLeft: (
        <Header
          onPress={() => navigation.navigate("GoInfo")}
          title="Go"
        />
      ),
      headerRight: (
        <MenuButton
          onPress={() => navigation.navigate("MainMenu")}
        />
      )
    };
  };

  render() {
    return (
      <MessagingScreen/>
    );
  }
}
