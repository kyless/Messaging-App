import React, { Component } from "react";

import MenuButton from "@root/components/MenuButton";
import Header from "@root/components/Header";
import MessagingScreen from "@root/components/MessagingScreen";
import Colors from "@root/constants/Colors";

export default class ShareScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: {
        backgroundColor: Colors.share,
      },
      headerLeft: (
        <Header
          title="Share"
          onPress={() => navigation.navigate("ShareInfo")}
        />
      ),
      headerRight: (
        <MenuButton
          onPress={() => navigation.navigate("MainMenu")}
        />
      )
    };
  };

  render() {
    return (
      <MessagingScreen/>
    );
  }
}
