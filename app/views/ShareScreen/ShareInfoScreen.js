import React, { Component } from "react";

import InfoScreen from "@root/components/InfoScreen/InfoScreen";
import Images from "@root/assets/images/index";
import Colors from "@root/constants/Colors";

export default class ShareInfoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      explanation: "The purpose of the this chanel is to share our possessions " +
      "with each other and to bear each other’s burdens.  It’s ok to admit a need. " +
      "We are a church family after all!  Generation after generation, we are there " +
      "for each other, just like the Acts church.",
      verse: "Contribute to the needs of the saints and seek to show hospitality.",
      verseReference: "Romans 12:13",
    };
  }

  render() {
    return(
      <InfoScreen
        backgroundColor={Colors.share}
        explanationFontSize="22"
        explanationText={this.state.explanation}
        headerTitle="Share"
        imagePath={Images.share}
        navigation={this.props.navigation}
        verse={this.state.verse}
        verseContainerColor={Colors.shareTint}
        verseFontSize="20"
        verseReference={this.state.verseReference}
      />
    );
  }
}
