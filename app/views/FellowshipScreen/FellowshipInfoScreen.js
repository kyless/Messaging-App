import React, { Component } from "react";

import Images from "@root/assets/images/index";
import InfoScreen from "@root/components/InfoScreen/InfoScreen";
import Colors from "@root/constants/Colors";

export default class FellowshipInfoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      explanation: "The purpose of the this chanel is to schedule time to be with " +
      "each other and eat with each other.  It’s a great idea to invite outsiders " +
      "as well!  Use this place to talk about the where and when and be sure to thing " +
      "of everyone when sending an event invitation.",
      verse: "And they devoted themselves to the apostles' teaching and the " +
      "fellowship, to the breaking of bread and the prayers.",
      verseReference: "Acts 2:42",
    };
  }

  render() {
    return(
      <InfoScreen
        backgroundColor={Colors.fellowship}
        explanationFontSize="20"
        explanationText={this.state.explanation}
        headerTitle="Fellowship"
        imagePath={Images.fellowship}
        navigation={this.props.navigation}
        verse={this.state.verse}
        verseContainerColor={Colors.fellowshipTint}
        verseFontSize="20"
        verseReference={this.state.verseReference}
      />
    );
  }
}
