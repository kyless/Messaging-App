import React, { Component } from "react";

import Colors from "@root/constants/Colors";
import Header from "@root/components/Header";
import MenuButton from "@root/components/MenuButton";
import MessagingScreen from "@root/components/MessagingScreen";

export default class FellowshipScreen extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: {
        backgroundColor: Colors.fellowship,
      },
      headerLeft: (
        <Header
          onPress={() => navigation.navigate("FellowshipInfo")}
          title="Fellowship"
        />
      ),
      headerRight: (
        <MenuButton
          onPress={() => navigation.navigate("MainMenu")}
        />
      )
    };
  };

  render() {
    return (
      <MessagingScreen/>
    );
  }
}
