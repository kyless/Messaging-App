import React, { Component } from "react";

import InfoScreen from "@root/components/InfoScreen/InfoScreen";
import Images from "@root/assets/images/index";
import Colors from "@root/constants/Colors";

export default class PrayInfoScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      explanation: "The purpose of the this channel is to constantly be in prayer " +
      "for each other. Make this a safe place to share sensative needs while " +
      "avoiding gossip about others. Update your prayers to tell others how God " +
      "is working!",
      verse: "Therefore, confess your sins to one another and pray for one another, " +
      "that you may be healed. The prayer of a righteous person has great power " +
      "as it is working.",
      verseReference: "James 5:16",
    };
  }

  render () {
    return (
      <InfoScreen
        backgroundColor={Colors.prayer}
        explanationFontSize="22"
        explanationText={this.state.explanation}
        headerTitle="Prayer"
        imagePath={Images.prayer}
        navigation={this.props.navigation}
        verse={this.state.verse}
        verseContainerColor={Colors.prayerTint}
        verseFontSize="20"
        verseReference={this.state.verseReference}
      />
    );
  }
}
