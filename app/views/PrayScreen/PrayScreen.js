import React, { Component } from "react";

import MenuButton from "@root/components/MenuButton";
import MessagingScreen from "@root/components/MessagingScreen";
import Header from "@root/components/Header";
import Colors from "@root/constants/Colors";

export default class PrayScreen extends Component {

  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: {
        backgroundColor: Colors.prayer,
      },
      headerLeft: (
        <Header
          title="Prayer"
          onPress={() => navigation.navigate("PrayInfo")}
        />
      ),
      headerRight: (
        <MenuButton
          onPress={() => navigation.navigate("MainMenu")}
        />
      )
    };
  };

  render() {
    return (
      <MessagingScreen/>
    );
  }
}
