import React, { Component } from "react";

import InfoScreen from "@root/components/InfoScreen/InfoScreen";
import Images from "@root/assets/images/index";
import Colors from "@root/constants/Colors";

export default class GrowInfoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      explanation: "This is a great place to share what God has been teaching " +
      "you.  Share scripture verses, links to sermons, books, etc. ,",
      verse: "And so, from the day we heard, we have not ceased to pray for you, " +
      " asking that you may be filled with the knowledge of his will in all " +
      "spiritual wisdom and understanding, so as to walk in a manner worthy of " +
      "the Lord, fully pleasing to him, bearing fruit in every good work and " +
      "increasing in the knowledge of God.",
      verseReference: "Colossians 1:9-10",
    };
  }

  render() {
    return(
      <InfoScreen
        backgroundColor={Colors.grow}
        explanationFontSize="20"
        explanationText={this.state.explanation}
        headerTitle="Grow"
        imagePath={Images.grow}
        navigation={this.props.navigation}
        verse={this.state.verse}
        verseContainerColor={Colors.growTint}
        verseFontSize="20"
        verseReference={this.state.verseReference}
      />
    );
  }
}
