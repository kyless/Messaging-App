import React, { Component } from "react";

import MenuButton from "@root/components/MenuButton";
import MessagingScreen from "@root/components/MessagingScreen";
import Header from "@root/components/Header";
import Colors from "@root/constants/Colors";

export default class GrowScreen extends Component {
    static navigationOptions = ({navigation}) => {
      return {
        headerStyle: {
            backgroundColor: Colors.grow,
        },
        headerLeft: (
          <Header
            title="Grow"
            onPress={() => navigation.navigate("GrowInfo")}
          />
        ),
        headerRight: (
          <MenuButton
            onPress={() => navigation.navigate("MainMenu")}
          />
        )
      };
    };

    render() {
      return (
        <MessagingScreen/>
      );
    }
  }
