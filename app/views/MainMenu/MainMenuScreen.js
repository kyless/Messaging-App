import React, { Component } from "react";
import {
  View,
  StyleSheet,
  TouchableNativeFeedback,
  TouchableHighlight,
  Platform,
  Text,
  StatusBar,
} from "react-native";
import { Icon } from "expo";

export default class MainMenuScreen extends Component {
  render() {
    const {
      navigation,
    } = this.props;
    const Touchable =
      Platform.OS === "android" ? TouchableNativeFeedback : TouchableHighlight;
    return(
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
        />
        <Icon.Ionicons style={styles.closeIcon}
          name={ Platform.OS === "ios" ? "ios-close" : "md-close" }
          onPress={() => navigation.goBack()}
          size={40}
        />
        <View style={styles.container}>
          <Touchable style={styles.button}
            onPress={() => navigation.navigate("Group")}>
            <Text style={styles.text}>My Group</Text>
          </Touchable>
          <Touchable style={styles.button}
            title="Profile"
            onPress={() => navigation.navigate("Profile")}>
            <Text style={styles.text}>Profile</Text>
          </Touchable>
          <Touchable style={styles.button}
            title="Invite"
            onPress={() => navigation.navigate("Invite")}>
            <Text style={styles.text}>Invite</Text>
          </Touchable>
          <Touchable style={[styles.button, {borderBottomColor: "#000000"}]}
            title="About"
            onPress={() => navigation.navigate("About")}>
            <Text style={styles.text}>About</Text>
          </Touchable>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    borderBottomWidth: 2,
    borderBottomColor: "#979797",
    marginLeft: 50,
    marginRight: 50,
  },
  container: {
    backgroundColor: "#000000",
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
  },
  closeIcon: {
    alignSelf: "flex-end",
    color: "#F4F4F4",
    margin: 40,
  },
  text: {
    color: "#FFFFFF",
    fontSize: 30,
    margin: 20,
    textAlign: "center",
  },
});
