import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Linking,
  StatusBar,
} from "react-native";

import LogoIcon from "@root/components/LogoIcon";
import Colors from "@root/constants/Colors";
import Button from "@root/components/Button";
import MenuButton from "@root/components/MenuButton";

export default class AboutScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      aboutExplanation:
      "This app was designed and built to facilitate the edification of the " +
      "local church in such a way that intentionally guides churches to a " +
      "biblical understanding of a healthy church community.  This app is not " +
      "a social media platform for churches. We intend to encourage real world " +
      "community, instead of replacing it.  This was built completly by service " +
      "to church members looking to bless their community, and yours!",
      boldText: "Made for the church, by the church",
      issuesText: "Having issues?  You can share your questions or comments on our support forum.",
      link: "ChurchCommunityApp.com",
      supportTitle: "Support",
      supportForumLink: "Visit Support Forum",
      versionNumber: "V1.2",
    }
  }

  render() {
    return(
      <View style={styles.screen}>
         <StatusBar
          barStyle="dark-content"
        />
        <View style={styles.container}>
          <View style={styles.logoContainer}>
            <LogoIcon style={styles.logoIcon}/>
            <MenuButton buttonStyle={styles.menuButton}
            onPress={() => this.props.navigation.goBack()}/>
          </View>
          <View style={styles.bodyContainer}>
            <Text style={styles.boldText}>{this.state.boldText}</Text>
            <Text>{this.state.aboutExplanation}</Text>
            <Text style={styles.link}
              onPress={() => this.goToURL('http://google.com')}
            >
              {this.state.link}
            </Text>
          </View>
          <View style={styles.rateContainer}>
            <Button
              buttonStyle={styles.button}
              textStyle={styles.text}
              title="Rate this app"
            />
            <Text style={styles.boldText}>{this.state.supportTitle}</Text>
            <Text>{this.state.issuesText}</Text>
            <Text style={styles.link}
              onPress={() => this.goToURL('http://google.com')}
            >
              {this.state.supportForumLink}
            </Text>
            <Text>{this.state.versionNumber}</Text>
          </View>
        </View>
      </View>
    );
  }

  goToURL(url) {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log('Don\'t know how to open URI: ' + url);
      }
    });
  }
}

const styles = StyleSheet.create({
  bodyContainer: {
    flex: 1,
  },
  boldText: {
    fontWeight: "bold",
  },
  button: {
    alignItems: "center",
    backgroundColor: "#000000",
    borderRadius: 10,
    elevation: 4,
    flexDirection: "row",
    height: 50,
    justifyContent: "center",
  },
  container: {
    flexDirection: "column",
    flex: 1,
    margin: 15,
  },
  link: {
    color: Colors.link,
    textDecorationLine: "underline",
  },
  logoContainer: {
    flexDirection: "row",
    flex: 1,
    backgroundColor: "#AAAAAA"
  },
  logoIcon: {
    justifyContent: "center",
  },
  menuButton: {
    flexDirection: "column",
    alignItems: "flex-end",
    marginTop: 30,
    marginRight: 10,
  },
  rateContainer: {
    flex: 1,
  },
  screen: {
    backgroundColor: Colors.backgroundColor,
    flex: 1,
  },
  text: {
    color: "#FFFFFF",
    fontWeight: "bold",
    justifyContent: "center",
  },
});
